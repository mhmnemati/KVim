# KVim
This is my own vim config file you can use it free

## Installing
__It's very simple :)__

1. copy `.vimrc` to your `home` path
2. open vim with ignoring any message and error
3. Enter vim command `:call KVim_Install()`
4. restart vim

## Key Maps

* `cf`: (code format) -> format entire code
* `co`: (code open) -> unfold all blocks
* `cc`: (code close) -> fold all blocks
* `cd`: (code dirs) -> toggle nerdtree file manager


## Fonts
You can download better fonts from [NerdFonts](http://nerdfonts.com/)

## KPS
KPS is my own Project Creating, Managing, Developing, Documenting standard <br/>
you can create KPS project by `:call KVim_New()`

## Screenshots
![KVim1](scree/vim1.png)
![KVim2](scree/vim2.png)
![KVim3](scree/vim3.png)

## Requirements
1. Debian base GNU/Linux Distro
2. APT package manager
3. git package